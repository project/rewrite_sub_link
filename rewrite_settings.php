<?php
/**
 * @file
 * The URL rewrite settings file, inculdes some rewrite url mapping
 *
 * You can delete this file, and move or the content into settings.php,
 * or another settings file which settings.php can include it
 */


global $conf;

/**
 * Rewrite_sub_tail_mapping
 *
 * Commonly we rewrite node or user sublink to the same name,
 * such as
 *    user/1/media  -> robbin/media
 *
 * but if you want to change 'media' to other word,
 * such as,
 *    user/1/media  -> robbin/likes
 *
 * so, put the mapping to here.
 *
 */
$conf['rewrite_sub_link_tail_mapping'] = array(
  'media/video'   => 'videos',
  'media/audio'   => 'audio',
  'media/image'   => 'images',
  'media/article' => 'articles',
  'connection/following' => 'following',
  'connection/follower'  => 'followers',
);


/**
 * inbound link regex rewrite
 *
 * support regex and callback function
 *
 * @example
 *  '{robbin-zhao/(\w+)/}i' => 'user/1/home',
 *  '{robbin-zhao/(\w+)/}i' => 'user/$1/home', //$1 is a backreference
 *  '{robbin-zhao/(\w+)/}i' => 'my_custom_function'
 *
 */
$conf['rewrite_sub_link_inbound_regex'] = array(

);

/**
 * outbound link regex rewrite
 *
 * support regex and callback function
 *
 * @example
 *  '{^user/(\w+)/}i' => 'robbin',
 *  '{^user/(\w+)/}i' => '$1', //$1 is a backreference
 *  '{^user/(\w+)/}i' => 'my_custom_function'
 *
 */
$conf['rewrite_sub_link_outbound_regex'] = array(

);
