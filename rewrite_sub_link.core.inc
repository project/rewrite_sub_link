<?php

/**
 * @author robbin robbin.joe@gmail.com
 * @file
 *  rewrite_sub_link.core.inc
 *  implements drupal core url rewrite functions.
 *  this file need to be loaded before all the modules were loaded
 *  so it should be included by settings.php or other equivalent including
 */

/**
 * Enable the USER sub-link rewrite
 */
define('REWRITE_SUB_LINK_USER', variable_get('rewrite_sub_link_user', TRUE));

/**
 * Enable the NODE sub-link rewrite
 */
define('REWRITE_SUB_LINK_NODE', variable_get('rewrite_sub_link_node', TRUE));

/**
 * The max deep sub-link
 */
define('REWRITE_SUB_LINK_LOOP_MAX', variable_get('rewrite_sub_link_loop_max', 2));

/**
 * Include rewrite settings
 */
$rewrite_settings_file = dirname(__FILE__) . '/rewrite_settings.php';
if (file_exists($rewrite_settings_file)) {
  include_once $rewrite_settings_file;
}


/**
 * Rewrite all the sub_link for the inbound links of user|node
 */
function rewrite_sub_url_inbound_alter(&$result, $path, $path_language) {
  // Don't rewrite user/id/edit or node/id/edit page
  if (preg_match('{(user|node)/\d+/edit.*?}i', $path)) {
    return FALSE;
  }

  /**
   * Check if the path has been changed
  if ($path != $result) {
    return FALSE;
  }
  **/

  $args = explode('/', $path);
  // Make sure has the sub url
  if (count($args) > 1) {

    $links = array();
    $count = 0;
    while ($el = array_shift($args)) {
      // Make sure has sub tail
      if (++$count >= REWRITE_SUB_LINK_LOOP_MAX && count($args) == 0) {
        break;
      }
      $links[] = $el;
      $url     = join('/', $links);
      $source  = _rewrite_sub_link_inbound_lookup($url);

      if ($source) {
        $tail   = join('/', $args);
        $tail   = _rewrite_sub_link_tail('source', $tail);
        $result = $source . '/' . $tail;

        // Add this to tell global_redirect not to redirect this url again
        $_REQUEST['q'] = $result;
        break;
      }
    }
  }
}

/*
 * Rewrite all the sub_link for the outbound link of user|node
 */
function rewrite_sub_url_outbound_alter(&$path, &$options, $original_path) {

  // Don't rewrite user/id/edit or node/id/edit page
  if (preg_match('{(user|node)/\d+/edit.*?}i', $path)) {
    return FALSE;
  }

  // Rewrite user's sub tab url to seo-friendly url
  // such as, user/1/media --> robbin-zhao/media
  if (preg_match('{^(user|node)/(\d+)/(.+)}i', $path, $matches)) {
    $type = $matches[1];
    $id   = $matches[2];
    $tail = $matches[3];

    $alias = drupal_lookup_path('alias', "$type/$id");

    if ($alias && $alias != $path) {
      $tail = _rewrite_sub_link_tail('alias', $tail);

      if (REWRITE_SUB_LINK_USER && strtolower($type) == 'user') {
        $path = "$alias/$tail";
      }
      if (REWRITE_SUB_LINK_NODE && strtolower($type) == 'node') {
        $path = "$alias/$tail";
      }
    }
  }
}

function _rewrite_sub_link_inbound_lookup($url) {

  $source_url = drupal_lookup_path('source', $url);
  if ($source_url && $source_url != $url && preg_match('{^(user|node)/(\d+)}i', $source_url, $matches)) {
    $type = $matches[1];
    $id   = $matches[2];
    // Could not use drupal_strtolower, because this function was not loaded in this phase
    if (REWRITE_SUB_LINK_USER && strtolower($type) == 'user') {
      return "user/$id";
    }

    if (REWRITE_SUB_LINK_NODE && strtolower($type) == 'node') {
      return "node/$id";
    }
  }

  return FALSE;
}

/**
 * Get the tail map
 * @example
 *    user/1/media/video  -> user-name/videos
 *    so, map medie/video -> videos
 *
 * @global $conf
 * @param string $type 'source' or 'alias'
 * @param string $tail the tail of url
 */
function _rewrite_sub_link_tail($type = 'source', $tail) {
  global $conf;

  // Should be source => alias
  $tail_map = array();

  if (isset($conf['rewrite_sub_link_tail_mapping'])) {
    $tail_map = array_merge($tail_map, $conf['rewrite_sub_link_tail_mapping']);
  }

  if ($type == 'alias') {
    if (isset($tail_map[$tail])) {
      return $tail_map[$tail];
    }
  }
  elseif ($type == 'source') {
    $array = array_flip($tail_map);
    if (isset($array[$tail])) {
      return $array[$tail];
    }
  }

  return $tail;
}

/**
 * Use regex to rewrite outbound URL
 */
function rewrite_sub_regex_outbound_alter(&$path, &$options, $original_path) {
  global $conf;

  if (isset($conf['rewrite_sub_link_outbound_regex']) && is_array($conf['rewrite_sub_link_outbound_regex'])) {
    foreach ($conf['rewrite_sub_link_outbound_regex'] as $key => $val) {
      if (preg_match($key, $path, $matches)) {
        if (is_callable($val)) {
          call_user_func_array($val, array(&$path, &$options, $original_path, $matches));
        }
        else {
          $path = preg_replace($key, $val, $path);
        }
        // Break out when the url was match
        break;
      }
    }
  }
}

/**
 * Use regex to rewrite inbound URL
 */
function rewrite_sub_regex_inbound_alter(&$result, $path, $path_language) {
  global $conf;

  /**
   * Check if the path has been changed
   * If you don't want a path was changed by many function, check it in your custom function to avoid it
  if ($path != $result) {
    return FALSE;
  }
  */

  $old_result = $result;

  if (isset($conf['rewrite_sub_link_inbound_regex']) && is_array($conf['rewrite_sub_link_inbound_regex'])) {
    foreach ($conf['rewrite_sub_link_inbound_regex'] as $key => $val) {
      if (preg_match($key, $path, $matches)) {
        if (is_callable($val)) {
          call_user_func_array($val, array(&$result, $path, $path_language, $matches));
        }
        else {
          $result = preg_replace($key, $val, $result);
        }

        // Break out when the url was match
        break;
      }
    }

    if ($result != $old_result) {
      // If the path was changed, we need to change the REQUEST['q'] to avoid redirect by golbal_redirect
      $_REQUEST['q'] = $result;
    }
  }
}



//// Implement Drupal inbound/outbound URL

/**
 * Define custom_url_rewrite_inbound() if not defined already.
 * Put your custom inbound at end
 *
 * @see custom_url_rewrite_inbound()
 * @author robbin
 */
if (!function_exists('custom_url_rewrite_inbound')) {
  // Define constant to check if custom_url_write_inbound was used
  define('REWRITE_SUB_LINK_INBOUND_ENABLED', TRUE);

  function custom_url_rewrite_inbound(&$result, $path, $path_language) {
    // Call fb inbound alter if exists
    if (function_exists('fb_url_inbound_alter')) {
      fb_url_inbound_alter($result, $path, $path_language);
    }


    // Rewrite node/user
    rewrite_sub_url_inbound_alter($result, $path, $path_language);

    // Rewrite using regex
    /*
     * Use regex after sub_url alter, since inbound alters won't change the $path variable.
     * So, the function at the end will have more power
     */
    rewrite_sub_regex_inbound_alter($result, $path, $path_language);

    // Put your custom inbound here
  }
}

/**
 * Define custom_url_rewrite_outbound() if not defined already.
 * Put your custom outbound url at end
 *
 * @see custom_url_rewrite_outbound()
 * @author robbin
 */
if (!function_exists('custom_url_rewrite_outbound')) {
  // Define constant to check if custom_url_write_outbound was used
  define('REWRITE_SUB_LINK_OUTBOUND_ENABLED', TRUE);

  function custom_url_rewrite_outbound(&$path, &$options, $original_path) {
    // Call fb outbound alter if exists
    if (function_exists('fb_url_outbound_alter')) {
      if (!isset($options['fb_url_alter']) || $options['fb_url_alter']) {
        if (function_exists('fb_canvas_url_outbound_alter')) {
          fb_canvas_url_outbound_alter($path, $options, $original_path);
        }
        fb_url_outbound_alter($path, $options, $original_path);
      }
    }

    // Rwrite using regex
    rewrite_sub_regex_outbound_alter($path, $options, $original_path);

    // Rewrite node/user
    rewrite_sub_url_outbound_alter($path, $options, $original_path);

    // Put your custom outbound here
  }
}
